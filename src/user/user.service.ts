import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as argon2 from 'argon2';
import { User } from './entity/user.interface';
import { IUserCredentials } from '../dto/userCredentials.interface';
import { RpcException } from '@nestjs/microservices';

@Injectable()
export class UsersService {

  constructor(
    @InjectModel('User') private readonly userModel: Model<User>,
  ) {}


  async create(createUserDto: IUserCredentials): Promise<User> {
    try {
      const user = await this.userModel.create(createUserDto);
      return user
    } catch (e) {
      throw new RpcException({ message: "User with this credentials exists.", code: 6 })
    }
  }

  async verifyUser(params: IUserCredentials): Promise<User> {
    const user = await this.userModel.findOne({ email: params.email });
    const isVerified = await argon2.verify(user.password, params.password);
    return isVerified ? user : null;
  }
}
