import {
  Controller,
} from '@nestjs/common';
import { UsersService } from './user.service';
import { GrpcMethod } from '@nestjs/microservices';
import { IUserCredentials } from '../dto/userCredentials.interface';
import { IUserResponse } from '../dto/userResponse.interface';

@Controller('users')
export class UsersController {
  constructor(private readonly userService: UsersService) {}

  @GrpcMethod()
  async createUser(params: IUserCredentials): Promise<IUserResponse> {
    return await this.userService.create(params);
  }

  @GrpcMethod()
  async verifyUser(params: IUserCredentials): Promise<IUserResponse> {
    return this.userService.verifyUser(params);
  }
}
