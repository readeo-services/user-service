import * as mongoose from "mongoose"
import * as argon2 from 'argon2';
import { User } from "./user.interface"

export const UserSchema = new mongoose.Schema({
  email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    }
})

UserSchema.pre("save", async function() {
  if (!this.isModified("password")) return;

  const hash = await argon2.hash(this.get('password'));
  this.set('password', hash)
})

UserSchema.set('toJSON', {
  transform: (_doc: User, ret: User) => {
    return (({ _id, email }) => ({ _id, email }))(ret);
  },
});
