import { ClientOptions, Transport } from '@nestjs/microservices';
import { join } from 'path';

export const userMicroserviceOptions: ClientOptions = {
  transport: Transport.GRPC,
  options: {
    package: 'user',
    url: '0.0.0.0:5000',
    protoPath: join(__dirname, '../../src/proto/user.proto'),
  },
};

